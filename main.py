from library.api_calls import ApiCalls
import json

def main():
    # Authentication
    instance = 'https://<org>.atlassian.net'
    username = '<your e-mail to access Jira>'
    password = '<your API Token>'

    # Set Authentication
    api_call = ApiCalls()
    api_call.setAuth('basic', username=username, password=password)

    # Get all users call
    response = api_call.request('GET', instance + '/rest/api/3/user/search?username=%')
    # Contains an array of all users, if successful
    response_json = response.json()

    # Check for error
    if response.status_code != 200:
        error = response.json()
        print(error)
    else:
        for user in response_json:
            # To see all information available, uncomment the line below
            # print(user)
            print(user['displayName'], user['accountId'])

    
if __name__ == '__main__':
    main()